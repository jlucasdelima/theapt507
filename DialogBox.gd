extends Node2D

signal dialog_close

var text = ""
var id = 0
var time = 0

func _ready():
	get_node("ColorRect/Label").text = text
	if time != 0:
		get_node("Timer").wait_time = float(time)
		get_node("Timer").start()
	pass

func _input(event):
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT and event.pressed:
		return;
	if event.pressed:
		return;
	
	get_node("AnimationPlayer").play("Close")
	
	pass



func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name != "Close":
		return
	emit_signal("dialog_close", id)
	queue_free()
	pass # Replace with function body.


func _on_Timer_timeout():
	get_node("AnimationPlayer").play("Close")
	pass # Replace with function body.
