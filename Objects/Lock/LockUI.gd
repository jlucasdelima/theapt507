extends Sprite

signal result

export (Array) var password = [0,0,0,0]

onready var anim = get_node("AnimationPlayer")


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_Number1_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT:
		return;
	if event.pressed:
		return;
	
	var label = get_node("Numbers/Number1/Label").text
	label = String(int(label) + 1) if int(label) < 9 else String(0)
	get_node("Numbers/Number1/Label").text = label
	pass # Replace with function body.


func _on_Number2_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT:
		return;
	if event.pressed:
		return;
	
	var label = get_node("Numbers/Number2/Label").text
	label = String(int(label) + 1) if int(label) < 9 else String(0)
	get_node("Numbers/Number2/Label").text = label
	pass # Replace with function body.


func _on_Number3_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT:
		return;
	if event.pressed:
		return;
	
	var label = get_node("Numbers/Number3/Label").text
	label = String(int(label) + 1) if int(label) < 9 else String(0)
	get_node("Numbers/Number3/Label").text = label
	pass # Replace with function body.


func _on_Number4_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT:
		return;
	if event.pressed:
		return;
	
	var label = get_node("Numbers/Number4/Label").text
	label = String(int(label) + 1) if int(label) < 9 else String(0)
	get_node("Numbers/Number4/Label").text = label
	pass # Replace with function body.


func _on_Area2D_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT:
		return;
	if event.pressed:
		return;
		
	var numbers = get_node("Numbers").get_children()
	for i in numbers.size():
		if int(numbers[i].get_node("Label").text) != password[i]:
			anim.play("error")
			return
	
	anim.play("success")
	pass # Replace with function body.


func _on_AnimationPlayer_animation_finished(anim_name):
	emit_signal("result", true if anim_name != "error" else false)
	queue_free()
	pass # Replace with function body.
