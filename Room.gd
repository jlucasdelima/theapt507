extends Node2D

signal change_scene

onready var views = get_node("Views").get_children()
onready var arrows = get_node("Arrows").get_children()

var activeView = 0 setget setActiveView
var clickReleased = true
var currentAudio = 0
var keyT = true
onready var audios = get_node("Audios").get_children()

func _ready():
	views[activeView].visible = true
	pass # Replace with function body.

func _process(delta):
	if Input.is_key_pressed(KEY_1):
		if not keyT:
			return
			
		$GameTime.emit_signal("timeout")
		keyT = false
	else:
		print("False")
		keyT = true
	
	if Input.is_key_pressed(KEY_M):
		emit_signal("change_scene", "museu")
		
	pass

func setActiveView(value):
	views[activeView].visible = false
	
	activeView = value
	
	if activeView < 0:
		activeView = views.size() - 1
	elif activeView >= views.size():
		activeView = 0
	
	views[activeView].visible = true
	pass

func _on_LeftArrow_input_event(viewport, event, shape_idx):
	
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT:
		return;
	if event.pressed:
		return;
	
	setActiveView(activeView + 1)
	
	pass # Replace with function body.


func _on_RightArrow_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT and event.pressed:
		return;
	if event.pressed:
		return;
	
	setActiveView(activeView - 1)
	
	pass # Replace with function body.


func _on_Switch_switch_click(active):
	
	get_node("Darkness").visible = active
	pass # Replace with function body.

func _on_GameTime_timeout():
	audios[currentAudio].stop()
	currentAudio += 1
	
	if currentAudio >= audios.size():
		return
	
	audios[currentAudio].play()
	var timer = $GameTime
	timer.start()
	pass # Replace with function body.
