extends Node2D

onready var CSCarArrivingObj = preload("res://CutScenes/Arriving/CSCarArriving.tscn")
onready var RoomObj = preload("res://Scenes/Room.tscn")
onready var MenuObj = preload("res://Scenes/Menu/Menu.tscn")
onready var MuseuObj = preload("res://Scenes/Museu.tscn")

onready var dialogObj = preload("res://Objects/DialogBox/DialogBox.tscn")

onready var transition = get_node("Transition")

onready var activeScene = CSCarArrivingObj.instance()
var nextScene = ""

onready var scenes = {
	"CSCarArriving": CSCarArrivingObj,
	"Menu": MenuObj,
	"Room": RoomObj,
	"museu": MuseuObj
}

# Called when the node enters the scene tree for the first time.
func _ready():
	add_child(activeScene)
	activeScene.connect("change_scene", self,"_change_scene")
	activeScene.connect("show_dialog", self, "_show_dialog")
	transition.connect("transition_out_finish", self, "_transition_out_finish")
	pass # Replace with function body.

func _change_scene(new_scene):
	nextScene = new_scene
	transition.play("black_fade_out")
	pass

func _transition_out_finish():
	activeScene.queue_free()
	activeScene = scenes.get(nextScene).instance()
	add_child(activeScene)
	activeScene.connect("change_scene", self,"_change_scene")
	activeScene.connect("show_dialog", self, "_show_dialog")
	transition.play("black_fade_in")
	pass

func _show_dialog(text, id, obj, callback, time = 0):
	var dialog = dialogObj.instance()
	dialog.text = text
	dialog.time = time
	dialog.id = id
	dialog.connect("dialog_close", obj, callback)
	add_child(dialog)
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
