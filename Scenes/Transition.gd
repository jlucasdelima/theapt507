extends Node2D

signal transition_out_finish

onready var anim = get_node("TransitionAnim")

var time_elapsed = 0
var time_trigger = 0
var currentTransition = ""

var signal_to_call = ""

var transitions = {
	"black_fade_out": "out",
	"black_fade_in": "in"
}

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func play(transition, time = 1):
	anim.play(transition, -1, time)
	pass

func _on_TransitionAnim_animation_finished(anim_name):
	if transitions.get(anim_name) == "out":
		emit_signal("transition_out_finish")
	pass # Replace with function body.
