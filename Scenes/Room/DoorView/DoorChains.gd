extends Sprite

onready var lockPasswordObj = preload("res://Objects/Lock/LockUI.tscn")

func _ready():
	pass


func _on_Area2D_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT and event.pressed:
		return;
	if event.pressed:
		return;
	for child in get_children():
		if child.name == "LockUI":
			return
	
	var instance = lockPasswordObj.instance()
	instance.password = [2,3,1,1]
	instance.connect("result", self, "_lock_result")
	add_child(instance)
	
	
	pass # Replace with function body.

func _lock_result(result):
	if result:
		queue_free()
	pass
