extends Sprite

signal switch_click

var active = true

func _ready():
	pass

func _on_Area2D_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT:
		return;
	if event.pressed:
		return;
	
	active = !active
	if !active:
		get_node("AudioStreamPlayer2D").play()
	emit_signal("switch_click", active)
	pass # Replace with function body.
