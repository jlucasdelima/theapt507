extends Sprite

onready var frames = [
	preload("res://Scenes/Room/WindowView/Wardrobe.png"),
	preload("res://Scenes/Room/WindowView/WardrobeOpened.png"),
	preload("res://Scenes/Room/WindowView/WardrobeGaveta.png")
]

var cframe = 0

func _ready():
	pass

func _on_FrontArea_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT:
		return;
	if event.pressed:
		return;
	cframe += 1
	if cframe >= frames.size():
		cframe = 0
		
	texture = frames[cframe]
	pass # Replace with function body.
