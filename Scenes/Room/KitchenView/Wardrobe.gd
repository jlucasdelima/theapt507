extends Sprite

onready var frames = [
	preload("res://Scenes/Room/KitchenView/Wardrobe.png"),
	preload("res://Scenes/Room/KitchenView/WardrobeBoth.png"),
	preload("res://Scenes/Room/KitchenView/WardrobeLeft.png"),
	preload("res://Scenes/Room/KitchenView/WardrobeRight.png")
]

var right = false
var left  = false

var cframe = 0

func _ready():
	pass

func updateObj():
	if right and left:
		texture = frames[1]
		return
	elif !right and !left:
		texture = frames[0]
		return
	elif right:
		texture = frames[3]
		return
	else:
		texture = frames[2]
		return
	pass

func _on_Left_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT:
		return;
	if event.pressed:
		return;
	
	left = !left
	updateObj()
	pass # Replace with function body.


func _on_Right_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT:
		return;
	if event.pressed:
		return;
		
	right = !right
	updateObj()
	pass # Replace with function body.
