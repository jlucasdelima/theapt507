extends Sprite

onready var frames = [
	preload("res://Scenes/Room/KitchenView/Sink.png"),
	preload("res://Scenes/Room/KitchenView/SinkOpened.png")
]

var cframe = 0

func _ready():
	pass

func _on_BottomArea_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT:
		return;
	if event.pressed:
		return;
	
	cframe = int(!cframe)
	texture = frames[cframe]
	pass # Replace with function body.
