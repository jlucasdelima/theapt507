extends Sprite

onready var frames = [
	preload("res://Scenes/Room/KitchenView/Cooker.png"),
	preload("res://Scenes/Room/KitchenView/CookerOpened.png")
]

onready var fire = get_node("Fire")

var cframe = 0

func _ready():
	pass

func _on_FurnaceArea_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT:
		return;
	if event.pressed:
		return;
	
	cframe = int(!cframe)
	texture = frames[cframe]
	pass # Replace with function body.


func _on_TopArea_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT:
		return;
	if event.pressed:
		return;
		
	fire.visible = !fire.visible
	pass # Replace with function body.
