extends Node2D

signal change_scene
signal show_dialog

var dialogs = [
	"I received a letter some days ago, requesting my services for repairing a very old clock, seems extremely important for the owner... ",
	"I didn't knew this town, it's too far from where I live, but I need the money, so I decided to take the job.",
	"Even on the darkness it's possible to see the old building.",
	"The place seems suffering from neglect, and the smell isn't nice, hope the money worth it...",
	"The door is ajar... Excuse-me, I'm the the clockmaker, I'm going in..."
]

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func change_scene():
	emit_signal("change_scene", "Menu")
	pass

func show_dialog(index):
	emit_signal("show_dialog", dialogs[index], index, self, "_dialog_close", 5)
	pass

func _input(event):
	if not event is InputEventMouseButton:
		return;
	if event.button_index != BUTTON_LEFT and event.pressed:
		return;
	if event.pressed:
		return;
	
	change_scene()
	pass

func _dialog_close(id):
	var index = id + 1
	if index >= dialogs.size():
		return
	emit_signal("show_dialog", dialogs[index], index, self, "_dialog_close", 5)
	pass
